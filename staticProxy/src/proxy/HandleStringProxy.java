package proxy;

import service.HandleString;
import service.impl.HandleStringImpl;

public class HandleStringProxy implements HandleString {

    private HandleString target;

    public HandleStringProxy() {
        target = new HandleStringImpl();
    }

    @Override
    public String doTransform() {
        return target.doTransform().trim();
    }

    @Override
    public String doTransform2() {
        return null;
    }
}
