package test;

import proxy.HandleStringProxy;
import service.HandleString;

public class Test {
    public static void main(String[] args) {
        HandleString target = new HandleStringProxy();
        System.out.println("Result:"+target.doTransform());
    }
}
/*
代理模式主要是控制对某个特定对象访问，而装饰模式主要是为了给对象添加行为。
 */
/*
静态代理与装饰者设计模式的区别：
1. 装饰者的使用目的是增强目标对象，而静态代理的目的是保护和隐藏目标对象（见第8行代码，没有HandleStringImpl类）
    （隐藏了目标对象，进一步可以隐藏目标对象中的某些方法，可以通过在Proxy类中重写逻辑限制调用）
2. 所以体现在实现上，装饰者方式是在带参构造函数中获取目标对象，而静态代理是在无参构造函数直接创建。
 */
/*
静态代理的缺点：
1. 冗余，由于代理对象要实现与目标对象一致的接口，会产生过多的代理类
2. 不易维护，一旦接口增加方法，目标对象与代理对象都要进行修改
 */