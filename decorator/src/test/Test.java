package test;

import decorators.TrimHandleString;
import decorators.UppercaseHandleString;
import service.HandleString;
import service.impl.HandleStringImpl;

public class Test {
    public static void main(String[] args) {
        HandleString target = new HandleStringImpl();
        HandleString service = new TrimHandleString(target);
        HandleString service2 = new UppercaseHandleString(service);
        System.out.println("Result:"+service2.doTransform());
    }
}
