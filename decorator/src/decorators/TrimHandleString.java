package decorators;

import service.HandleString;

//只去掉前后空格
public class TrimHandleString extends HandleStringWrapper {

    public TrimHandleString(HandleString target) {
        super(target);
    }

    @Override
    public String doTransform() {
        return super.doTransform().trim();
    }
}
