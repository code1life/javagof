package decorators;

import service.HandleString;

//不做任何实质性工作
public class HandleStringWrapper implements HandleString {

    private HandleString target;

    public HandleStringWrapper(HandleString target) {
        this.target = target;
    }

    @Override
    public String doTransform() {
        return target.doTransform();
    }

    @Override
    public String doTransform2() {
        return null;
    }
}
