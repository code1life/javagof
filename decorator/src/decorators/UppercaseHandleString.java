package decorators;

import service.HandleString;

//只变为大写
public class UppercaseHandleString extends HandleStringWrapper {
    public UppercaseHandleString(HandleString target) {
        super(target);
    }

    @Override
    public String doTransform() {
        return super.doTransform().toUpperCase();
    }
}
