package proxy;

import service.UserService;
import service.impl.UserServiceImpl;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;

public class UserServiceJdkProxyFactory implements InvocationHandler {

    private UserService userService;

    public UserServiceJdkProxyFactory() {
        userService = new UserServiceImpl();
    }

    public UserService getUserServiceProxy() {
        //返回false，代理对象和被代理对象没有继承关系（cglib是继承关系），只是两者实现了相同的接口 (UserServiceImpl.class.getInterfaces())
        System.out.println((UserService) Proxy.newProxyInstance(UserServiceJdkProxyFactory.class.getClassLoader(), UserServiceImpl.class.getInterfaces(), this) instanceof UserServiceImpl);
        return (UserService) Proxy.newProxyInstance(UserServiceJdkProxyFactory.class.getClassLoader(), UserServiceImpl.class.getInterfaces(), this);
    }

    @Override
    public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
        System.out.println("打开事务");//相当于增强代码
        Object invoke = method.invoke(userService, args);//原有方法代码
        System.out.println("提交事务");//增强功能的代码
        return invoke;
    }
}
