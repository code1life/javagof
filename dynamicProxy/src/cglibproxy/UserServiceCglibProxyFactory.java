package cglibproxy;

import net.sf.cglib.proxy.Enhancer;
import net.sf.cglib.proxy.MethodInterceptor;
import net.sf.cglib.proxy.MethodProxy;
import service.UserService;
import service.impl.UserServiceImpl;

import java.lang.reflect.Method;

public class UserServiceCglibProxyFactory implements MethodInterceptor {
    public UserService getUserServiceProxy(){
        Enhancer enhancer = new Enhancer();
        enhancer.setSuperclass(UserServiceImpl.class);//设置对谁进行代理
        enhancer.setCallback(this);//设置代理做什么
        return (UserService) enhancer.create();//创建代理对象
    }

    @Override
    public Object intercept(Object o, Method method, Object[] args, MethodProxy methodProxy) throws Throwable {
        System.out.println("打开事务");//功能增强代码
        Object returnValue = methodProxy.invokeSuper(o, args);
        System.out.println("提交事务");
        return returnValue;
    }
}
