package test;

import proxy.UserServiceJdkProxyFactory;

public class Test {
    public static void main(String[] args) {
        new UserServiceJdkProxyFactory().getUserServiceProxy().save();
    }
}

/*
动态代理与静态代理的区别：
1. 静态代理在编译完成时生成一个实际的class
   动态代理是在运行时动态生成的，没有实际的class文件，而是在运行时动态生成类字节码并加载到JVM中
2. 动态代理对象不需要实现接口，目标对象必须实现接口
 */
